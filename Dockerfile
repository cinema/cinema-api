FROM maven:3.5.2-jdk-8 AS build 

WORKDIR /srv

COPY src ./src  
COPY pom.xml .  
RUN mvn -f ./pom.xml clean package


FROM openjdk:8-alpine

WORKDIR /srv

COPY --from=build /srv/target/cinema-api-0.0.1-SNAPSHOT.jar /srv/cinema-api-0.0.1-SNAPSHOT.jar

EXPOSE 9001

ENTRYPOINT ["java","-jar","/srv/cinema-api-0.0.1-SNAPSHOT.jar"]  
